
def is_criticality_balanced(temperature, neutrons_emitted):
	if temperature < 800 and neutrons_emitted > 500:
		return (temperature * neutrons_emitted) < 5e5
    	return False

print is_criticality_balanced(750, 600)