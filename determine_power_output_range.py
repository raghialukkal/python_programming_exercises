
def reactor_efficiency(voltage, current, theoretical_max_power):
    
    generated_power = voltage * current
    efficiency = (generated_power / theoretical_max_power) * 100
    if efficiency >= 80:
        return 'green'
    if efficiency >= 60:
        return 'orange'
    if efficiency >= 30:
        return 'red'
    return 'black'

	
print reactor_efficiency(200,50,15000)