
def fail_safe(temperature, neutrons_produced_per_second, threshold):
    
    criticality = temperature * neutrons_produced_per_second
    low_threshold = 0.9 * threshold
    high_threshold = 1.1 * threshold
    if temperature * neutrons_produced_per_second < low_threshold:
        return 'LOW'
    if low_threshold <= criticality <= high_threshold:
        return 'NORMAL'
    return 'DANGER'
print fail_safe(temperature=1000, neutrons_produced_per_second=30, threshold=5000)
